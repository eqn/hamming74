hamming74
=========

Tools for encoding and decoding data with Hamming(7,4)
error-correcting code.

The tools have currently been tested to work on OpenBSD.
