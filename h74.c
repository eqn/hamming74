#include <libgen.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "arg.h"
#include "util.h"

#define LEN(x) (sizeof(x) / sizeof(*(x)))
#define MAXLINE 8192
#define RESLEN 8
#define BFLAG 1
#define DFLAG 2
#define SFLAG 4

/* code generator matrix */
static const int G[7][4] = {
	{1, 1, 0, 1},
	{1, 0, 1, 1},
	{1, 0, 0, 0},
	{0, 1, 1, 1},
	{0, 1, 0, 0},
	{0, 0, 1, 0},
	{0, 0, 0, 1}
};
/* parity-check matrix */
static const int H[3][7] = {
	{1, 0, 1, 0, 1, 0, 1},
	{0, 1, 1, 0, 0, 1, 1},
	{0, 0, 0, 1, 1, 1, 1}
};
char *argv0;

static char *
decode(char *s)
{
	int i;
	int j;
	int e;
	int z[3] = {0};
	char *r = malloc(sizeof(char) * 5);

	if (!r)
		eprintf("malloc\n");

	for (i = 0; i < 3; i++) {
		for (j = 0; j < 7; j++) {
			z[i] += H[i][j] * (s[j] - '0');
		}
		z[i] %= 2;
	}
	if ((e = sum(z, LEN(z))) != 0)
		flip(s, e - 1);

	r[0] = s[2];
	r[1] = s[4];
	r[2] = s[5];
	r[3] = s[6];
	r[4] = 0;

	return r;
}

static char *
encode(char *s)
{
	int p1, p2, p4;
	char *r = malloc(sizeof(char) * 8);

	if (!r)
		eprintf("malloc\n");

	p1 = ((s[0] - '0') + (s[1] - '0') + (s[3] - '0')) % 2;
	p2 = ((s[0] - '0') + (s[2] - '0') + (s[3] - '0')) % 2;
	p4 = ((s[1] - '0') + (s[2] - '0') + (s[3] - '0')) % 2;

	r[0] = p1 + '0';
	r[1] = p2 + '0';
	r[2] = s[0];
	r[3] = p4 + '0';
	r[4] = s[1];
	r[5] = s[2];
	r[6] = s[3];
	r[7] = 0;

	return r;
}

static int
hamming(FILE *f, char *name, int flags, unsigned int blksize,
	char *(*fn)(char *))
{
	int line, dsum, i, j, ret;
	char s[MAXLINE], *sp, res[RESLEN];

	line = 0;
	ret = 0;

	while (fgets(s, MAXLINE, f)) {
		line++;
		s[strlen(s) - 1] = 0; /* Remove trailing newline */
		if (!strlen(s))
			continue;
		trim(s);
		if (!isclean(s)) {
			wprintf("%s:%d: unknown characters, skipping\n",
				name, line);
			ret++;
			continue;
		}
		sp = s;
		while (strlen(sp) >= blksize) {
			strlcpy(res, (*fn)(sp), RESLEN);

			if (flags & BFLAG)
				printf("%s%s", res, ((flags & DFLAG) ||
					(flags & SFLAG)) ? " " : "");
			if (flags & DFLAG) {
				dsum = 0;
				for (i = strlen(res) - 1, j = 0; i >= 0; i--, j++)
					dsum += (int)((res[i] - '0') * pow(2, j));
				printf("%d%s", dsum, (flags & BFLAG) ?
					"\n" : " ");
			}
			sp += blksize;
		}
		if (!((flags & BFLAG) && (flags & DFLAG)))
			putchar('\n');
	}

	return ret;
}

static void
usage(void)
{
	eprintf("usage: %s [-bds] [file ...]\n", argv0);
}

int
main(int argc, char **argv)
{
	int i, flags, ret, blksize;
	FILE *f;
	char *(*fn)(char *);
	char *name;

	argv0 = argv[0];
	flags = 0;
	ret = 0;

	ARGBEGIN {
	case 'b':
		flags |= BFLAG;
		break;
	case 'd':
		flags |= DFLAG;
		break;
	case 's':
		flags |= SFLAG;
		break;
	case 'v':
		printf("%s-%s\n", argv0, VERSION);
		return 0;
	default:
		usage();
	} ARGEND;

	if (!(flags & BFLAG) && !(flags & DFLAG))
		flags |= BFLAG;

	name = basename(argv0);
	if (strncmp(name, "h74encode", 9) == 0) {
		fn = encode;
		blksize = 4;
	} else if (strncmp(name, "h74decode", 9) == 0) {
		fn = decode;
		blksize = 7;
	} else
		eprintf("%s: unrecognized name, aborting\n", name);

	if (argc == 0)
		ret += hamming(stdin, "<stdin>", flags, blksize, fn);
	else for (i = 0; i < argc; i++) {
		if (!(f = fopen(argv[i], "r"))) {
			wprintf("error reading file: %s\n", argv[i]);
			ret++;
			continue;
		}
		if (argc > 1)
			printf("==> %s <==\n", argv[i]);
		ret += hamming(f, argv[i], flags, blksize, fn);
		if (i < argc - 1)
			putchar('\n');
		fclose(f);
	}

	return ret;
}
