VERSION = 0.1

include config.mk

OBJ = h74.o util.o
DECBIN = h74decode
ENCBIN = h74encode

all: ${DECBIN} ${ENCBIN}

${DECBIN} ${ENCBIN}: ${OBJ}
	${CC} -o $@ ${OBJ} ${LDLIBS}

h74.o: arg.h util.h
util.o: util.h

install: all
	mkdir -p ${DESTDIR}${PREFIX}/bin
	cp -f ${DECBIN} ${ENCBIN} ${DESTDIR}${PREFIX}/bin
	mkdir -p ${DESTDIR}${MANPREFIX}/man1
	cp -f ${DECBIN}.1 ${ENCBIN}.1 ${DESTDIR}${MANPREFIX}/man1

uninstall:
	rm -f ${DESTDIR}${PREFIX}/bin/${DECBIN} \
		${DESTDIR}${PREFIX}/bin/${ENCBIN}
	rm -f ${DESTDIR}${MANPREFIX}/man1/${DECBIN}.1 \
		${DESTDIR}${MANPREFIX}/man1/${ENCBIN}.1

clean:
	rm -f ${DECBIN} ${ENCBIN} ${OBJ}

.PHONY: all install uninstall clean
