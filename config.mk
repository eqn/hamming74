PREFIX = /usr/local
MANPREFIX = ${PREFIX}/man

CPPFLAGS = -DVERSION=\"${VERSION}\"
CFLAGS = -Wall -Wextra -std=c99 -pedantic
LDLIBS = -lm
