#include <math.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "util.h"

void
eprintf(char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
	exit(1);
}

void
wprintf(char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
}

void
flip(char *s, int i)
{
	int b;

	b = s[i] - '0';
	s[i] = !b + '0';
}

int
isclean(char *s)
{
	int i;

	for (i = 0; s[i]; i++)
		if (s[i] != '0' && s[i] != '1')
			return 0;
	return 1;
}

int
sum(int *v, size_t len)
{
	unsigned int i, s = 0;

	for (i = 0; i < len; i++)
		s += v[i] * (int)pow(2, i);
	return s;
}

char *
trim(char *s)
{
	int i, j;
	char *sp = s;

	for (i = 0, j = 0; sp[i]; i++) {
		if (sp[i] == ' ' || sp[i] == '\t')
			continue;
		s[j++] = sp[i];
	}
	s[j] = 0;
	return s;
}
