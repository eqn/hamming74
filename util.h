void	 eprintf(char *, ...);
void	 wprintf(char *, ...);

void	 flip(char *, int);
int	 isclean(char *);
int	 sum(int *, size_t);
char	*trim(char *);
